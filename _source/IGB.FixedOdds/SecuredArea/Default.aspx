﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Page.master" Title="Fixed Odds App" CodeBehind="Default.aspx.cs" Inherits="IGB.FixedOdds.Index" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="Main" Runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 col-md-12 col-sm-12 calculators-container">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 calculator" id="calculator1">
	                <h3>Calculator 1</h3>
	                <label>Trap 1:</label>
	                <div class="odds-cell-duo" id="calc1duo1">
		                <input type="number" min="0" class="grid-cell odds-cell left" id="txtTrap1L"/>/<input type="number"  min="0" class="grid-cell odds-cell right" id="txtTrap1R"/>
	                </div>
	                <br/>
	                <label>Trap 2:</label>	
	                <div class="odds-cell-duo" id="calc1duo2">
		                <input type="number" min="0" class="grid-cell odds-cell left" id="txtTrap2L"/>/<input type="number"  min="0" class="grid-cell odds-cell right" id="txtTrap2R"/>
	                </div>
	                <br/>
	                <label>Trap 3:</label>	
	                <div class="odds-cell-duo" id="calc1duo3">
		                <input type="number" min="0" class="grid-cell odds-cell left" id="txtTrap3L"/>/<input type="number"  min="0" class="grid-cell odds-cell right" id="txtTrap3R"/>
	                </div>
	                <br/>
	                <label>Trap 4:</label>	
	                <div class="odds-cell-duo" id="calc1duo4">
		                <input type="number" min="0" class="grid-cell odds-cell left" id="txtTrap4L"/>/<input type="number"  min="0" class="grid-cell odds-cell right" id="txtTrap4R"/>
	                </div>
	                <br/>
	                <label>Trap 5:</label>	
	                <div class="odds-cell-duo" id="calc1duo5">
		                <input type="number" min="0" class="grid-cell odds-cell left" id="calc1txtTrap5L"/>/<input type="number"  min="0" class="grid-cell odds-cell right" id="calc1txtTrap5R"/>
	                </div>
	                <br/>
	                <label>Trap 6:</label>	
	                <div class="odds-cell-duo" id="calc1duo6">
		                <input type="number" min="0" class="grid-cell odds-cell left" id="calc1txtTrap6L"/>/<input type="number"  min="0" class="grid-cell odds-cell right" id="calc1txtTrap6R"/>
	                </div>
	                <br/>
	                <input type="button" class="btn calculate-btn btn-success" id="btnCalculate1" value="Calculate"/><br/>
	                <input type="text" class="grid-cell percentage-cell" placeholder="%" id="txtCalculated1" readonly="true"/>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 calculator" id="calculator2">
	                <h3>Calculator 2</h3>
	                <label>Trap 1:</label>
	                <div class="odds-cell-duo" id="calc2duo1">
		                <input type="number" min="0" class="grid-cell odds-cell left" id="calc2txtTrap1L"/>/<input type="number"  min="0" class="grid-cell odds-cell right" id="calc2txtTrap1R"/>
	                </div>
	                <br/>
	                <label>Trap 2:</label>	
	                <div class="odds-cell-duo" id="calc2duo2">
		                <input type="number" min="0" class="grid-cell odds-cell left" id="calc2txtTrap2L"/>/<input type="number"  min="0" class="grid-cell odds-cell right" id="calc2txtTrap2R"/>
	                </div>
	                <br/>
	                <label>Trap 3:</label>	
	                <div class="odds-cell-duo" id="calc2duo3">
		                <input type="number" min="0" class="grid-cell odds-cell left" id="calc2txtTrap3L"/>/<input type="number"  min="0" class="grid-cell odds-cell right" id="calc2txtTrap3R"/>
	                </div>
	                <br/>
	                <label>Trap 4:</label>	
	                <div class="odds-cell-duo" id="calc2duo4">
		                <input type="number" min="0" class="grid-cell odds-cell left" id="calc2txtTrap4L"/>/<input type="number"  min="0" class="grid-cell odds-cell right" id="calc2txtTrap4R"/>
	                </div>
	                <br/>
	                <label>Trap 5:</label>	
	                <div class="odds-cell-duo" id="calc2duo5">
		                <input type="number" min="0" class="grid-cell odds-cell left" id="calc2txtTrap5L"/>/<input type="number"  min="0" class="grid-cell odds-cell right" id="calc2txtTrap5R"/>
	                </div>
	                <br/>
	                <label>Trap 6:</label>	
	                <div class="odds-cell-duo" id="calc2duo6">
		                <input type="number" min="0" class="grid-cell odds-cell left" id="calc2txtTrap6L"/>/<input type="number"  min="0" class="grid-cell odds-cell right" id="calc2txtTrap6R"/>
	                </div>
	                <br/>
	                <input type="button" class="btn calculate-btn btn-success" id="btnCalculate2" value="Calculate"/><br/>
	                <input type="text" class="grid-cell percentage-cell" placeholder="%" id="txtCalculated2" readonly="true"/><br/>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 calculator" id="calculator3">
	                <h3>Calculator 3</h3>
	                <label>Trap 1:</label>
	                <div class="odds-cell-duo" id="calc3duo1">
		                <input type="number" min="0" class="grid-cell odds-cell left" id="calc3txtTrap1L"/>/<input type="number"  min="0" class="grid-cell odds-cell right" id="calc3txtTrap1R"/>
	                </div>
	                <br/>
	                <label>Trap 2:</label>	
	                <div class="odds-cell-duo" id="calc3duo2">
		                <input type="number" min="0" class="grid-cell odds-cell left" id="calc3txtTrap2L"/>/<input type="number"  min="0" class="grid-cell odds-cell right" id="calc3txtTrap2R"/>
	                </div>
	                <br/>
	                <label>Trap 3:</label>	
	                <div class="odds-cell-duo" id="calc3duo3">
		                <input type="number" min="0" class="grid-cell odds-cell left" id="calc3txtTrap3L"/>/<input type="number"  min="0" class="grid-cell odds-cell right" id="calc3txtTrap3R"/>
	                </div>
	                <br/>
	                <label>Trap 4:</label>	
	                <div class="odds-cell-duo" id="calc3duo4">
		                <input type="number" min="0" class="grid-cell odds-cell left" id="calc3txtTrap4L"/>/<input type="number"  min="0" class="grid-cell odds-cell right" id="calc3txtTrap4R"/>
	                </div>
	                <br/>
	                <label>Trap 5:</label>	
	                <div class="odds-cell-duo" id="calc3duo5">
		                <input type="number" min="0" class="grid-cell odds-cell left" id="calc3txtTrap5L"/>/<input type="number"  min="0" class="grid-cell odds-cell right" id="calc3txtTrap5R"/>
	                </div>
	                <br/>
	                <label>Trap 6:</label>	
	                <div class="odds-cell-duo" id="calc3duo6">
		                <input type="number" min="0" class="grid-cell odds-cell left" id="calc3txtTrap6L"/>/<input type="number"  min="0" class="grid-cell odds-cell right" id="calc3txtTrap6R"/>
	                </div>
	                <br/>
	                <input type="button" class="btn calculate-btn btn-success" id="btnCalculate3" value="Calculate"/><br/>
	                <input type="text" class="grid-cell percentage-cell" placeholder="%" id="txtCalculated3" readonly="true"/>
                </div>
            </div>
            
            <div class="col-lg-4 col-md-12 col-sm-12 upcoming-races-container">
                <div id="divToday" runat="server">
                    <asp:Label ID="lblRacesToday" CssClass="races-header" runat="server">Races Today</asp:Label><span id="glyShowHideRacesToday" class="glyphicon glyphicon-chevron-up gly"></span>
                    <div id="divRacesToday">
                        <asp:Repeater ID="rptTracksToday" runat="server" OnItemDataBound="rptTracks_ItemDataBound">
                            <ItemTemplate>
                                <div class="tracks-rpt">
                                    <asp:Label ID="lblTrack" CssClass="lbl-track-name" runat="server" Text='<%# Eval("NamTrack") %>'></asp:Label>
                                    <asp:Label ID="lblRace" CssClass="lbl-race" runat="server" Text="Race"></asp:Label>
                                    <asp:DropDownList ID="ddlRaces" CssClass="ddl-race" runat="server" DataTextField="NmbRace" DataValueField="IdtRace"></asp:DropDownList>
                                    <asp:Button ID="btnEdit" CssClass="btn edit-btn btn-warning" Text="Edit" runat="server"></asp:Button>
                                </div>
                                <div class="clearfix"></div>
	                        </itemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <div id="divOther">
                    <asp:Label ID="lblOtherRaces" CssClass="races-header" runat="server">Other Races</asp:Label><span id="glyShowHideOtherRaces" class="glyphicon glyphicon-chevron-up gly"></span>
                    <div id="divOtherRaces" runat="server">
                        <asp:Label ID="lblRacesDate" CssClass="date-label" runat="server">Date:</asp:Label>
                        <asp:TextBox ID="txtRacesDate" CssClass="date-picker" ClientIDMode="Static" runat="server"></asp:TextBox>
                        <asp:Button ID="btnUpdateOtherRaces" CssClass="btn update-races-btn btn-info" Text="Update" runat="server" OnClick="btnUpdateOtherRaces_Click" />
                        <asp:Repeater ID="rptOtherTracks" runat="server" OnItemDataBound="rptTracks_ItemDataBound">
                            <ItemTemplate>
                                <div class="tracks-rpt">
                                    <asp:Label ID="lblTrack" CssClass="lbl-track-name" runat="server" Text='<%# Eval("NamTrack") + ": " %>'></asp:Label>
                                    <asp:Label ID="lblRace" CssClass="lbl-race" runat="server" Text="Race"></asp:Label>
                                    <asp:DropDownList ID="ddlRaces" CssClass="ddl-race" runat="server" DataTextField="NmbRace" DataValueField="IdtRace"></asp:DropDownList>
                                    <asp:Button ID="btnEdit" CssClass="btn edit-btn btn-warning" Text="Edit" runat="server"></asp:Button>
                                </div>
                                <div class="clearfix"></div>
	                        </itemTemplate>
                        </asp:Repeater>
                        <div class="clearfix"></div>
                        <asp:Label ID="lblNoRaces" runat="server" CssClass="bg-info">No available races for date.</asp:Label>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row streams-row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 streams-column">
                <h3>Streaming Links</h3>
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                    <input type="submit" class="btn stream-btn btn-info" id="btnTRLStream" value="Tralee" />
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                    <input type="submit" class="btn stream-btn btn-info" id="btnMGRStream" value="Mullingar" />
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                    <input type="submit" class="btn stream-btn btn-info" id="btnYGLStream" value="Youghal" />
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                    <input type="submit" class="btn stream-btn btn-info" id="btnSPKStream" value="Shelbourne Park" />
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                    <input type="submit" class="btn stream-btn btn-info" id="btnHRXStream" value="Harolds Cross" />
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                    <input type="submit" class="btn stream-btn btn-info" id="btnLMKStream" value="Limerick" />
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                    <input type="submit" class="btn stream-btn btn-info" id="btnGLYStream" value="Galway" />
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                    <input type="submit" class="btn stream-btn btn-info" id="btnCRKStream" value="Cork" />
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                    <input type="submit" class="btn stream-btn btn-info" id="btnWFDStream" value="Waterford" />
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                    <input type="submit" class="btn stream-btn btn-info" id="btnCMLStream" value="Clonmel" />
                </div>
            </div>
        </div>
        <div class="row">
            <asp:Button type="submit" class="btn btn-danger" id="btnLogout" Text="Logout" runat="server" OnClick="btnLogout_Click" />
        </div>
    </div>
    <script type="text/javascript" src="../ClientFiles/Default.js"></script>
</asp:Content>