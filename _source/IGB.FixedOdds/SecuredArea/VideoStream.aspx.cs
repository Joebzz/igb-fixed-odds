﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

namespace IGB.FixedOdds.VideoStream
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        public void Page_Load(object sender, EventArgs e)
        {
            string toteApi = "http://videocontrol.datatote.net/igb_stream.php?username={0}&password={1}&stadium={2}";
            string username = "igbweb"; // enter your username here
            string password = "3z46c349RPgW364d"; // enter your password here
            string stadium = Request.QueryString["track"]; ; // get the track from the query string

            string finalToteUrl = string.Format(toteApi, username, password, stadium);
            string retUrl = GetVideoURL(finalToteUrl);

            videoStreamLink.InnerText = retUrl;
        }

        public string GetVideoURL(string requestUrl)
        {
            string responseString = "";

            using (WebClient client = new WebClient())
            {
                responseString = client.DownloadString(requestUrl);
            }

            int pFrom = responseString.IndexOf("url=\"") + "url=\"".Length;
            int pTo = responseString.LastIndexOf("\"");

            String resp = responseString.Substring(pFrom, pTo - pFrom);

            return resp;
        }
    }
}