﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IGB.FixedOdds.SecuredArea
{
    public partial class EditFixedOdds : System.Web.UI.Page
    {
        double toteOddsTotal = 0, customerPriceTotal = 0, fixedPriceTotal = 0;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        protected void gridRunners_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkRow = (e.Row.FindControl("chkFPLocked") as CheckBox);

                if (chkRow.Checked)
                    e.Row.Cells[4].CssClass = "tote-live-odds-strike";
                
                string oldLPString = DataBinder.Eval(e.Row.DataItem, "ValToteLiveOdds").ToString();
                string oldCPString = DataBinder.Eval(e.Row.DataItem, "ValToteCustomerOdds").ToString();
                string oldFPString = DataBinder.Eval(e.Row.DataItem, "ValToteFixedOdds").ToString();

                if (!String.IsNullOrEmpty(oldLPString))
                    toteOddsTotal += CalculateFraction(oldLPString);

                if (!String.IsNullOrEmpty(oldFPString))
                    fixedPriceTotal += CalculateFraction(oldFPString);

                if (!String.IsNullOrEmpty(oldCPString))
                    customerPriceTotal += CalculateFraction(oldCPString);

            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            { 
                e.Row.Cells[2].Text = string.Format("{0:N2}%", fixedPriceTotal);
                e.Row.Cells[4].Text = string.Format("{0:N2}%", toteOddsTotal);
                e.Row.Cells[5].Text = string.Format("{0:N2}%", customerPriceTotal);
            }
        }

        private void LoadData()
        {
            int raceId;
            // get the track from the query string
            if (int.TryParse(Request.QueryString["idtRace"], out raceId) && raceId != 0)
            {
                DataSet dsNextPrevRaces = GetNextPrevRace(raceId);

                if (dsNextPrevRaces.Tables.Count > 0)
                {
                    int nextRaceId, prevRaceId;
                    if (!int.TryParse(dsNextPrevRaces.Tables[0].Rows[0]["PrevRaceID"].ToString(), out prevRaceId) || prevRaceId == 0)
                    {
                        prevRace.Enabled = false;
                        savePrevRace.Enabled = false;
                    }
                    else
                    {
                        prevRace.Enabled = true;
                        savePrevRace.Enabled = true;
                        string url = Request.Url.AbsolutePath;
                        string updatedQueryString = "?idtRace=" + prevRaceId.ToString();
                        prevRace.PostBackUrl = url + updatedQueryString;
                    }
                    if (!int.TryParse(dsNextPrevRaces.Tables[0].Rows[0]["NextRaceID"].ToString(), out nextRaceId) || nextRaceId == 0)
                    {
                        nextRace.Enabled = false;
                        saveNextRace.Enabled = false;
                    }
                    else
                    {
                        nextRace.Enabled = true;
                        saveNextRace.Enabled = true;
                        string url = Request.Url.AbsolutePath;
                        string updatedQueryString = "?idtRace=" + nextRaceId.ToString();
                        nextRace.PostBackUrl = url + updatedQueryString;
                    }
                }

                DataSet ds = GetRunners(raceId);
                if (ds.Tables.Count > 0)
                {
                    trackName.InnerText = ds.Tables[0].Rows[0]["NamTrack"].ToString();
                    raceDate.InnerText = DateTime.Parse(ds.Tables[0].Rows[0]["DatMeeting"].ToString()).ToShortDateString();
                    raceNumber.InnerText = ds.Tables[0].Rows[0]["NmbRace"].ToString();

                    gridRunners.DataSource = ds;
                    gridRunners.DataBind();
                }
            }
            else
            {
                string jScript = "<script>window.close();</script>";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "keyClientBlock", jScript);
            }
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            int raceId;
            // get the track from the query string
            if (int.TryParse(Request.QueryString["idtRace"], out raceId) && raceId != 0)
            {
                SaveRunners(raceId);
            }
            else
            {
                string jScript = "<script>window.close();</script>";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "keyClientBlock", jScript);
            }
        }

        protected void RefreshButton_Click(object sender, EventArgs e)
        {
            chkAutoRefresh.Checked = true;
            LoadData();
        }

        protected void ChangeRaceButton_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        protected void SaveChangeRaceButton_Click(object sender, EventArgs e)
        {
            LinkButton button = (LinkButton)sender;

            int raceId;
            // get the track from the query string
            if (int.TryParse(Request.QueryString["idtRace"], out raceId) && raceId != 0)
            {
                SaveRunners(raceId);
                                
                DataSet dsNextPrevRaces = GetNextPrevRace(raceId);

                if (dsNextPrevRaces.Tables.Count > 0)
                {
                    int nextRaceId, prevRaceId;
                    if (int.TryParse(dsNextPrevRaces.Tables[0].Rows[0]["PrevRaceID"].ToString(), out prevRaceId) && prevRaceId != 0 && button.ID == "savePrevRace")
                    {
                        string url = Request.Url.AbsolutePath;
                        string updatedQueryString = "?idtRace=" + prevRaceId.ToString();
                        Response.Redirect(url + updatedQueryString);
                    }
                    if (int.TryParse(dsNextPrevRaces.Tables[0].Rows[0]["NextRaceID"].ToString(), out nextRaceId) && nextRaceId != 0 && button.ID == "saveNextRace")
                    {
                        string url = Request.Url.AbsolutePath;
                        string updatedQueryString = "?idtRace=" + nextRaceId.ToString();
                        Response.Redirect(url + updatedQueryString);
                    }
                }
            }
            else
            {
                string jScript = "<script>window.close();</script>";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "keyClientBlock", jScript);
            }

            LoadData();
        }
        
        protected void DoneButton_Click(object sender, EventArgs e)
        {
            int raceId;
            // get the track from the query string
            if (int.TryParse(Request.QueryString["idtRace"], out raceId) && raceId != 0)
            {
                SaveRunners(raceId);
            }
            string jScript = "<script>window.close();</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "keyClientBlock", jScript);
        }
        
        double CalculateFraction(string fraction)
        {
            double result;

            if (double.TryParse(fraction, out result))
            {
                return result;
            }

            string[] split = fraction.Split(new char[] { ' ', '/' });

            if (split.Length == 2 || split.Length == 3)
            {
                int a, b;

                if (int.TryParse(split[0], out a) && int.TryParse(split[1], out b))
                {
                    if (split.Length == 2)
                    {
                        return (double)b / (a + b) * 100;
                    }
                }
            }

            return 0;
        }
        
        /// <summary>
        /// Get runners for the provided race ID
        /// </summary>
        /// <returns>Dataobject</returns>
        public DataSet GetRunners(int raceId)
        {
            DataSet ds = new DataSet("Runners");
            string connectionString = ConfigurationManager.AppSettings["CN"];
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand sqlComm = new SqlCommand("intapp_FixedOddsGetToteRunners", conn);
                sqlComm.Parameters.AddWithValue("@IdtRace", raceId);

                sqlComm.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;

                da.Fill(ds);
            }
            return ds;
        }

        public void SaveRunners(int raceId)
        {
            DataSet ds = GetRunners(raceId);

            for (int i = 0; i < gridRunners.Rows.Count; i++)
            {
                DateTime datMeeting = DateTime.Parse(ds.Tables[0].Rows[i]["DatMeeting"].ToString());
                string abvTrack = ds.Tables[0].Rows[i]["AbvTrack"].ToString();
                int nmbRace, nmbTrap;
                if (!int.TryParse(ds.Tables[0].Rows[i]["NmbRace"].ToString(), out nmbRace)
                    || !int.TryParse(ds.Tables[0].Rows[i]["NmbTrap"].ToString(), out nmbTrap))
                {
                    return;
                }
                string oldFPString = ds.Tables[0].Rows[i]["ValToteFixedOdds"].ToString();
                string oldFPChecked = ds.Tables[0].Rows[i]["FlgFixedPriceLocked"].ToString();

                TextBox txtFPIdL = (TextBox)gridRunners.Rows[i].FindControl("ValToteFixedOddsL");
                TextBox txtFPIdR = (TextBox)gridRunners.Rows[i].FindControl("ValToteFixedOddsR");

                CheckBox chkFPLocked = (CheckBox)gridRunners.Rows[i].FindControl("chkFPLocked");
                string valToteFixedOdds = "";
                if (txtFPIdL.Text != "" && txtFPIdR.Text != "")
                    valToteFixedOdds = txtFPIdL.Text + "/" + txtFPIdR.Text;

                bool flgFixedPriceLocked = chkFPLocked.Checked;
                if (oldFPChecked != flgFixedPriceLocked.ToString() || oldFPString != valToteFixedOdds)
                    SaveToteRunner(datMeeting, abvTrack, nmbRace, nmbTrap, valToteFixedOdds, flgFixedPriceLocked);

            }

            LoadData();            
        }

        /// <summary>
        /// Save the Runner details
        /// </summary>
        /// <returns>Int</returns>
        public void SaveToteRunner(DateTime datMeeting, string abvTrack, int nmbRace, int nmbTrap, string valToteFixedOdds, bool flgFixedPriceLocked)
        {
            string connectionString = ConfigurationManager.AppSettings["CN"];
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand sqlComm = new SqlCommand("intapp_FixedOddsSaveToteRunners", conn))
                {
                    conn.Open();
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.Clear();
                    sqlComm.Parameters.AddWithValue("@datMeeting", datMeeting.ToString("dd MMM yyy"));
                    sqlComm.Parameters.AddWithValue("@abvTrack", abvTrack);
                    sqlComm.Parameters.AddWithValue("@nmbRace", nmbRace);
                    sqlComm.Parameters.AddWithValue("@nmbTrap", nmbTrap);
                    sqlComm.Parameters.AddWithValue("@FlgFixedPriceLocked", flgFixedPriceLocked);
                    sqlComm.Parameters.AddWithValue("@ValToteFixedOdds", valToteFixedOdds);

                    sqlComm.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }

        /// <summary>
        /// Get next race and previous race ID's in the meeting for the provided race ID
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetNextPrevRace(int raceId)
        {
            DataSet ds = new DataSet("NextPrevRaceIDs");
            string connectionString = ConfigurationManager.AppSettings["CN"];
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand sqlComm = new SqlCommand("intapp_FixedOddsGetNextPrevRaceIDs", conn);
                sqlComm.Parameters.AddWithValue("@IdtRace", raceId);

                sqlComm.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;

                da.Fill(ds);
            }
            return ds;
        }
    }
}