﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Page.master" Title="Video Stream" CodeBehind="VideoStream.aspx.cs" Inherits="IGB.FixedOdds.VideoStream.WebForm1" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="Server">
    <link href="http://vjs.zencdn.net/4.12/video-js.css" rel="stylesheet" />
    <script src="http://vjs.zencdn.net/4.12/video.js"></script>
    <script src="https://github.com/videojs/videojs-contrib-media-sources/releases/download/v0.1.0/videojs-media-sources.js"></script>
    <script src="https://github.com/videojs/videojs-contrib-hls/releases/download/v0.11.2/videojs.hls.min.js"></script>
    <script src="../ClientFiles/jwplayer-7.2.4/jwplayer.js"></script>
    <script>jwplayer.key = "A1rAEObCE4XRlZij7iGCjMEEJbdZjUDfz2B/xA==";</script>
    
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="Main" runat="Server">
    <label id="videoStreamLink" class="url-link" style="display: none" runat="server"></label>
    <div id="pcPlayer">The player should load here<br />
        <a href="http://www.adobe.com/go/getflashplayer">
            <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a>
        
    </div>
    <div id='phonePlayer'>
        <video id="videoStream" class="video-js vjs-default-skin" controls preload="auto" width="640" height="464">
            <source id="videosource" src='' type="application/x-mpegURL" runat="server" />
            <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
        </video>
    </div>
    <script type="text/javascript" src="../ClientFiles/VideoStream.js"></script>
</asp:Content>
