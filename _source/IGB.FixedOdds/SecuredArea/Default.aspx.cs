﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IGB.FixedOdds
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {            
                DateTime tomorrow = DateTime.Now.AddDays(1);
                txtRacesDate.Text = tomorrow.ToString("d MMM yyy");
                DataSet dsToday = GetTracks(DateTime.Now.ToString("dd MMM yyy"));
                if (dsToday.Tables[0].Rows.Count > 0)
                {
                    rptTracksToday.DataSource = dsToday;
                    rptTracksToday.DataBind();
                }
                else
                {
                    divToday.Visible = false;
                }
                
                DataSet dsOtherRaces = GetTracks(txtRacesDate.Text);
                if (dsOtherRaces.Tables[0].Rows.Count > 0)
                {
                    lblNoRaces.Visible = false;
                    rptOtherTracks.DataSource = dsOtherRaces;
                    rptOtherTracks.DataBind();
                }
                else
                    lblNoRaces.Visible = true;
            }
        }

        /// <summary>
        /// Get runners for the provided race ID
        /// </summary>
        /// <returns>Dataobject</returns>
        public DataSet GetTracks(string date)
        {
            DataSet ds = new DataSet("Tracks");
            string connectionString = ConfigurationManager.AppSettings["CN"];
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand sqlComm = new SqlCommand("intapp_FixedOddsGetTracksRacing", conn);
                sqlComm.Parameters.AddWithValue("@Dat", date);

                sqlComm.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;

                da.Fill(ds);
            }
            return ds;
        }

        /// <summary>
        /// Get runners for the provided race ID
        /// </summary>
        /// <returns>Dataobject</returns>
        public DataSet GetRaces(string date, int idtTrack)
        {
            DataSet ds = new DataSet("Races");
            string connectionString = ConfigurationManager.AppSettings["CN"];
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand sqlComm = new SqlCommand("intapp_FixedOddsGetTracksRaces", conn);
                sqlComm.Parameters.AddWithValue("@Dat", date);
                sqlComm.Parameters.AddWithValue("@IdtTrack", idtTrack);

                sqlComm.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;

                da.Fill(ds);
            }
            return ds;
        }

        protected void rptTracks_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView dr = (DataRowView)e.Item.DataItem;
                int trackID = Convert.ToInt16(dr["IdtTrack"]);
                DateTime dt = Convert.ToDateTime(dr["DatMeeting"]);

                DropDownList selectList = e.Item.FindControl("ddlRaces") as DropDownList;
                if (selectList != null)
                {
                    selectList.DataSource = GetRaces(dt.ToString("dd MMM yyy"), trackID); //your datasource
                    selectList.DataBind();
                }

            }
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
        }
    
        protected void btnUpdateOtherRaces_Click(object sender, EventArgs e)
        {
            DataSet dsOtherRaces = GetTracks(txtRacesDate.Text);
            if (dsOtherRaces.Tables[0].Rows.Count > 0)
            {
                lblNoRaces.Visible = false;
                rptOtherTracks.DataSource = dsOtherRaces;
                rptOtherTracks.DataBind();
            }
            else
                lblNoRaces.Visible = true;
        }
    }
}