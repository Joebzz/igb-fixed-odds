﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Page.master" Title="Edit Odds" CodeBehind="EditFixedOdds.aspx.cs" Inherits="IGB.FixedOdds.SecuredArea.EditFixedOdds" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="Main" runat="Server">
    
    <div class="container-fluid header-info">            
        <div class="row race-changer">
            <nav>
                <ul class="pager">
                    <li>
                        <asp:LinkButton ID="prevRace" CssClass="race-pager-link" runat="server" Text="Previous" OnClick="ChangeRaceButton_Click" />
                    </li>
                    <li>
                        <asp:LinkButton ID="savePrevRace" CssClass="race-pager-link" runat="server" Text="Save & Previous" OnClick="SaveChangeRaceButton_Click"  />
                    </li>
                    <li>
                        <asp:LinkButton ID="saveNextRace" CssClass="race-pager-link" runat="server" Text="Save & Next" OnClick="SaveChangeRaceButton_Click" />
                    </li>
                    <li>
                        <asp:LinkButton ID="nextRace" CssClass="race-pager-link" runat="server" Text="Next" OnClick="ChangeRaceButton_Click" />
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row">
            <div class="col-md-12 refresh">
                <asp:Checkbox ID="chkAutoRefresh" ClientIDMode="Static" runat="server" />
                <span id="lastreload"></span>
            </div>
        </div>
        <div class="row race-info">
            <div class="col-md-12">
                <div class="col-md-3 info-container">
                    <label>Track Name:</label>
                    <span id="trackName" runat="server"></span>
                </div>
                <div class="col-md-3 info-container">
                    <label>Race Date:</label>
                    <span id="raceDate" runat="server"></span>
                </div>
                <div class="col-md-3 info-container">
                    <label>Race Number:</label>
                    <span id="raceNumber" runat="server"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row" id="page-input">
            <div class="col-md-12 edit-fixedodds">
                <asp:GridView ID="gridRunners" runat="server" AutoGenerateColumns="False" Width="100%" OnRowDataBound="gridRunners_RowDataBound" ShowFooter="true">
                    <HeaderStyle CssClass="runners-header-row" />
                    <Columns>
                        <asp:BoundField DataField="NmbTrap" HeaderText="Trap" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="NamGreyhound" HeaderText="Name" ItemStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderText="FP">
                            <HeaderStyle Width="90" />
                            <ItemStyle Width="90" HorizontalAlign="Center" />
                            <ItemTemplate>
                                <div class="odds-cell-duo" id="calc1duo1">
                                    <asp:TextBox ID="ValToteFixedOddsL" type="number" autocomplete="off" MaxLength="2" CssClass="grid-cell odds-cell left" Text=<%# Eval("ValToteFixedOdds").ToString().Split('/').First() %> runat="server" />
                                    /
		                            <asp:TextBox ID="ValToteFixedOddsR" type="number" autocomplete="off" MaxLength="2" CssClass="grid-cell odds-cell right" Text=<%# Eval("ValToteFixedOdds").ToString().Split('/').Last() %> runat="server" />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lock FP">
                            <HeaderStyle Width="70" />
                            <ItemStyle Width="70" HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkFPLocked" CssClass="check-lock-fp" TabIndex="-1" runat="server" Checked='<%# Eval("FlgFixedPriceLocked") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ValToteLiveOdds" HeaderText="Live Price" ItemStyle-CssClass="tote-live-odds" ItemStyle-Width="100" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="ValToteCustomerOdds" HeaderText="Customer Price" ItemStyle-Width="120" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="NamToteReserve" HeaderText="Tote Reserve Name" ItemStyle-CssClass="Center" />
                    </Columns>
                    <FooterStyle CssClass="runners-footer-row" />
                </asp:GridView>
            </div>
        </div>
    
        <div class="row">
            <div class="fixed-odds-buttons col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <asp:Button ID="btnSave" CssClass="btn btn-primary save-btn" runat="server" Text="Save" OnClick="SaveButton_Click"></asp:Button>
            </div>
            <div class="fixed-odds-buttons col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <asp:Button ID="btnDone" CssClass="btn btn-success done-btn" runat="server" Text="Save & Done" OnClick="DoneButton_Click"></asp:Button>
            </div>
            <div class="fixed-odds-buttons col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <asp:Button ID="btnRefresh" CssClass="btn btn-info refresh-btn" runat="server" Text="Refresh" OnClick="RefreshButton_Click"></asp:Button>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="../ClientFiles/EditFixedOdds.js"></script>
</asp:Content>
