﻿// Javascript to prevent anything but a fraction from being entered into the Fixed Price Column
$(document).ready(function () {
    $('.odds-cell').each(function (e) {
        // Bring back the previously entered figures
        var parentDivId = $(this).parent().parent().attr('id');
        if (localStorage['stored_' + parentDivId + '_' + this.id] !== undefined) {
            this.value = localStorage['stored_' + parentDivId + '_' + this.id];
            $(this).change();
        }
    });
    $('.odds-cell').keydown(function (e) {
        // Allow: backspace, delete, tab, escape and /
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 110, 191]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $('#txtRacesDate').datepicker({
        dateFormat: "d M yy"
    });
    $('.calculate-btn').click(Calculate);
    $('.calculator').each(CalculateAll);
    $('.stream-btn').click(OpenStream);
    $('.edit-btn').click(OpenRace);
    $('.gly').click(ShowHideRaces);    
});
function CalculateAll() {
    var btnCalculate = $(this).find('.calculate-btn');
    btnCalculate.trigger('click');
}
function Calculate() {
    var calculatorDivId = $(this).parent().attr('id');
    var curPer = localStorage['stored_' + calculatorDivId + '_percentage'];
    if (curPer !== undefined) {
        localStorage.removeItem('stored_' + calculatorDivId + '_percentage');
    }
    $('#' + calculatorDivId + ' .odds-cell-duo').each(CalculatePercentage);
}
function CalculatePercentage() {
    var duoDivId = this.id;
    var calculatorDivId = $(this).parent().attr('id');

    var prevVal = "";
    var currentVal = 0;
    $('#' + calculatorDivId + ' #' + duoDivId + ' .odds-cell').each(function (e) {
        if (localStorage['stored_' + calculatorDivId + '_percentage'] !== undefined) {
            currentVal = localStorage['stored_' + calculatorDivId + '_percentage'];
        }

        var val = this.value;
        var valCombined = "";
        if (val !== "" && prevVal !== "" && val !== "0") {
            valCombined = prevVal + "/" + val;
        }
        else if (val != "" && val !== "0") {
            prevVal = val;
            localStorage['stored_' + calculatorDivId + '_' + this.id] = this.value;
        }
        if (valCombined !== "") {
            var fractVal = CalculateFraction(valCombined);
            var result = +currentVal + +fractVal;
            currentVal = parseFloat(Math.round(result * 100) / 100).toFixed(2);
            var calCell = $('#' + calculatorDivId + ' .percentage-cell');
            if (currentVal == "NaN")
                calCell.val("ERROR");
            else
                calCell.val(currentVal + '%');

            localStorage['stored_' + calculatorDivId + '_' + this.id] = this.value;
            localStorage['stored_' + calculatorDivId + '_percentage'] = currentVal;
        }
    });
}
function CalculateFraction(fraction) {
    var result = parseFloat(fraction);

    var split = fraction.split("/");

    result = parseInt(split[1], 10) / (parseInt(split[0], 10) + parseInt(split[1], 10));

    return result * 100;
}
function OpenStream() {
    var track = $(this).val();
    var pageurl = "VideoStream.aspx?track=" + $.trim(track.toLowerCase());
    PopupCenter(pageurl, track + " Stream", 736, 415);
    
    return false;
}
function OpenRace() {
    var ddlRace = $(this).prev();
    var trackName = $(this).prev().prev().val();
    var idtRace = ddlRace.val();
    var title = "Edit - " + trackName + ": RaceID - " + idtRace;
    var pageurl = "EditFixedOdds.aspx?idtRace=" + idtRace;
    PopupCenter(pageurl, title, 780, 435);
    
    return false;
}
function PopupCenter(url, title, w, h) {
    // Fixes dual-screen position                         Most browsers      Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }
}
function ShowHideRaces()
{
    if ($(this).next().is(":visible")) {
        $(this).next().hide();

        $(this).removeClass('glyphicon-chevron-up');
        $(this).addClass('glyphicon-chevron-down');
    }
    else {
        $(this).next().show();

        $(this).removeClass('glyphicon-chevron-down');
        $(this).addClass('glyphicon-chevron-up');
    }
}