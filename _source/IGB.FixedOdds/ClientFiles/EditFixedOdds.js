﻿var refreshPage;
var wasChecked = 0;

function InitTimeout() {
    refreshPage = setTimeout(function () {
        clearTimeout(refreshPage);
        var refreshButton = $('.refresh-btn');
        refreshButton.click();
    }, 5000);
}

function StopTimeout() {
    clearTimeout(refreshPage);
}

// Javascript to prevent anything but a fraction from being entered into the Fixed Price Column
$(document).ready(function () {
    $('.race-pager-link').each(function (e) {
        if ($(this).is('[disabled=disabled]') || $(this).hasClass("aspNetDisabled")) 
            $(this).parent().addClass("disabled");        
        else
            $(this).parent().removeClass("disabled");
    });
    $('.odds-cell').keydown(function (e) {
        // Allow: backspace, delete, tab, escape and /
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 110, 191]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $("#page-input input[type=number]").bind('focus', function () {
        StopTimeout();
        $("#chkAutoRefresh").prop('checked', false);
    })

    $('.refresh-btn').bind('click', function () {
        InitTimeout();
    })

    $("#chkAutoRefresh").unbind('click').bind('click', function () {

        if ($("#chkAutoRefresh").prop('checked')) {
            $.cookie("autorefresh", "1");
            InitTimeout();
            wasChecked = 1;
        }
        else {
            $.cookie("autorefresh", "0");
            StopTimeout();
            wasChecked = 0;
        }
    });

    var defaultRefresh = 0;
    if (typeof $.cookie("autorefresh") === 'undefined' || $.cookie("autorefresh") == "1") {
        defaultRefresh = 1;
        wasChecked = 1;
    }

    if (defaultRefresh) {
        InitTimeout();
        $("#chkAutoRefresh").prop('checked', true);
    }

    var currentdate = new Date();
    var datetime = "Last Refresh: " + currentdate.getDate() + "/"
                    + (currentdate.getMonth() + 1) + "/"
                    + currentdate.getFullYear() + " @ "
                    + ('0' + currentdate.getHours()).slice(-2) + ":"
                    + ('0' + currentdate.getMinutes()).slice(-2) + ":"
                    + ('0' + currentdate.getSeconds()).slice(-2);

    $("#lastreload").html(datetime);
});