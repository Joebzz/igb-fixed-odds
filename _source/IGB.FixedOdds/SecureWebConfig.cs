﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace IGB.FixedOdds
{
    public class SecureWebConfig
    {
        public static void EncryptConnectionString()
        {
            try
            {
                Configuration confg = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
                ConfigurationSection confStrSect = confg.GetSection("appSettings");
                if (confStrSect != null)
                {
                    confStrSect.SectionInformation.ProtectSection("RSAProtectedConfigurationProvider");
                    confg.Save();
                }

                ConfigurationSection confStrSessionSect = confg.GetSection("system.web/sessionState");
                if (confStrSessionSect != null)
                {
                    confStrSessionSect.SectionInformation.ProtectSection("RSAProtectedConfigurationProvider");
                    confg.Save();
                }
            }
            catch (Exception ex)
            {

            }
        }

        public static void DecryptConnectionString()
        {
            Configuration confg = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
            ConfigurationSection confStrSect = confg.GetSection("appSettings");
            if (confStrSect != null && confStrSect.SectionInformation.IsProtected)
            {
                confStrSect.SectionInformation.UnprotectSection();
                confg.Save();
            }

            ConfigurationSection confStrSessionSect = confg.GetSection("system.web/sessionState");
            if (confStrSessionSect != null)
            {
                confStrSessionSect.SectionInformation.UnprotectSection();
                confg.Save();
            }
        }
    }
}