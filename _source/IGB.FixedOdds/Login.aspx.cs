﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IGB.FixedOdds
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect("SecuredArea");
            }

            else 
            {
                Configuration confg = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
                ConfigurationSection confStrSect = confg.GetSection("appSettings");
                if (!System.Diagnostics.Debugger.IsAttached && !confStrSect.SectionInformation.IsProtected)
                {
                    SecureWebConfig.EncryptConnectionString();

                    userMsg("Encrypt successful");
                }
            }
        }

        private bool ValidateUser(string userName, string passWord)
        {
            bool validPass = false;

            if ((userName == WebConfigurationManager.AppSettings["adminUsername"].ToString()) && (passWord == WebConfigurationManager.AppSettings["adminPassword"].ToString()))
            {
                validPass = true;
            }

            return validPass;
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (ValidateUser(txtUserName.Value, txtUserPass.Value))
            {
                FormsAuthentication.SetAuthCookie(txtUserName.Value, true);
                Response.Redirect("SecuredArea");
            }
            else
                userMsg("Incorrect user and / or password incorrect. Please try again.");
        }

        protected void btnDecrypt_Click(object sender, EventArgs e)
        {
            if (ValidateUser(txtUserName.Value, txtUserPass.Value))
            {
                SecureWebConfig.DecryptConnectionString();
                userMsg("Decrypt successful");
            }
            else
                userMsg("Incorrect user and / or password incorrect. Please try again.");
        }

        protected void userMsg(string msg)
        {
            lblMsg.Text = msg;
        }
    }
}