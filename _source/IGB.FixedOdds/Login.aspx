﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="Page.master" Title="Login" CodeBehind="Login.aspx.cs" Inherits="IGB.FixedOdds.login" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="Main" runat="Server">
    <div class="loginClass container">
        <div class="col-lg-4 col-lg-offset-5 col-md-4 col-md-offset-5 col-sm-6 col-sm-offset-4 col-xs-12 col-xs-offset-0 ">
            <div class="row">
                <h2 class="login-header">Please Login:</h2>
            </div>
            <div class="row">
                <label for="txtUserName">Username:</label>
                <input id="txtUserName" name="txtUserName" type="text" placeholder="Username" runat="server" />
            </div>
            <div class="row">
                <label for="txtUserPass">Password:</label>
                <input id="txtUserPass" name="txtUserPass" type="password" placeholder="Password" runat="server" />
            </div>
            <div class="row">
                <asp:Button ID="btnLogin" CssClass="btn btn-success" runat="server" Text="Login" OnClick="btnLogin_Click" />                
                <asp:Button ID="btnDecrypt" CssClass="btn btn-warning" runat="server" Text="Decrypt" OnClick="btnDecrypt_Click" />                
            </div>
            <asp:Label ID="lblMsg" CssClass="bg-danger error-label" runat="server" />
        </div>
    </div>
</asp:Content>
